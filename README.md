doc2json
========

Convert DOC BBS binary flat files to JSON for importation into 21st century software.

Dependencies which may not be easily installable:

1. ctypes code generator, an old, dusty couple of scripts that came with the ctypes
   library before it became part of Python.
     http://starship.python.net/crew/theller/ctypes/old/codegen.html

2. gccxml: https://github.com/gccxml/gccxml

