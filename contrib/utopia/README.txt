Here's an example of the output from gccxml.  Here are some commands:

  654  h2xml.py defs.h -o defs.xml -I /Users/neuro/00nobak/uresc/vDOC-utopia-production
  658  cp defs.xml ~/PycharmProjects/doc2json/contrib/
  664  xml2py.py defs.xml -c -k s -r '^(?!.*darwin).*$' > defs.py
  665  cp defs.py ~/PycharmProjects/doc2json/contrib/

You'll have to hand-edit defs.py after this...
