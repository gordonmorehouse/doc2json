from distutils.core import setup

setup(
    name='doc2json',
    version='0.1.1a',
    author='Gordon Morehouse 0x1EEFFBA3',
    author_email='gordon@morehouse.me',
    packages=['doc2json'],
    url='https://github.com/gordon-morehouse/doc2json',
    license='GPL2',
    description='Attempt to read DOC (Dave\'s Own Citadel) BBS files and output JSON',
    long_description=open('README.md').read(),
    install_requires=[
        'http://svn.python.org/projects/ctypes/trunk/ctypeslib/',	# http://starship.python.net/crew/theller/ctypes/old/codegen.html
    ],
)
    
